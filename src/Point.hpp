//
// Created by jonathan on 2019-06-08.
//

#ifndef FRACTALS_POINT_HPP
#define FRACTALS_POINT_HPP

template<typename T>
class Point {
public:
    Point() = default;

    Point(T x, T y) : m_x(x), m_y(y) {}

    Point(const Point &x) = default;

    Point(Point &&x) noexcept = default;

    Point &operator+=(const Point &r) {
        m_x += r.m_x;
        m_y += r.m_y;
        return *this;
    }

    Point &operator-=(const Point &r) {
        m_x -= r.m_x;
        m_y -= r.m_y;
        return *this;
    }

    friend Point operator+(const Point &l, const Point &r) {
        return Point(l.m_x + r.m_x, l.m_y + r.m_y);
    }

    friend Point operator-(const Point &l, const Point &r) {
        return Point(l.m_x - r.m_x, l.m_y - r.m_y);
    }

    Point &operator=(const Point &o) {
        m_x = o.m_x;
        m_y = o.m_y;
        return *this;
    }

    T x() const { return m_x; }

    void x(T x) { m_x = x; }

    T y() const { return m_y; }

    void y(T y) { m_y = y; }

private:
    T m_x = 0;
    T m_y = 0;
};


#endif //FRACTALS_POINT_HPP
