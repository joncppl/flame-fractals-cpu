//
// Created by jonathan on 2019-06-08.
//

#ifndef FRACTALS_VARIATIONS_HPP
#define FRACTALS_VARIATIONS_HPP

#include "Point.hpp"

#include <cmath>

template<typename T>
T r2(const Point<T> &x) {
    return (x.x() * x.x() + x.y() * x.y());
}

template<typename T>
T r(const Point<T> &x) {
    return std::sqrt(r2(x));
}

template<typename T>
T theta(const Point<T> &x) {
    return std::atan2(x.x(), x.y());
}

template<typename T>
T phi(const Point<T> &x) {
    return std::atan2(x.y(), x.x());
}

template<typename T>
Point<T> variation0(const Point<T> &x) {
    return x;
}

template<typename T>
Point<T> variation1(const Point<T> &x) {
    return Point(std::sin(x.x()), std::sin(x.y()));
}

template<typename T>
Point<T> variation2(const Point<T> &x) {
    auto f = (T) 1.0 / r2(x);
    return Point(f * x.x(), f * x.y());
}

template<typename T>
Point<T> variation3(const Point<T> &x) {
    auto f = r2(x);
    return Point(x.x() * sin(f) - x.y() * cos(f), x.x() * cos(f) + x.y() * sin(f));
}

template<typename T>
Point<T> variation4(const Point<T> &x) {
    auto f = (T) 1.0 / r(x);
    return Point(f * ((x.x() - x.y()) * (x.x() + x.y())), f * (T) 2.0 * x.x() * x.y());
}

template<typename T>
Point<T> variation5(const Point<T> &x) {
    auto t = theta(x);
    auto f = r(x);
    return Point(t / (T) M_PI, f - 1);
}

template<typename T>
Point<T> variation6(const Point<T> &x) {
    auto t = theta(x);
    auto f = r(x);
    return Point(f * std::sin(t + f), f * std::cos(t - f));
}

template<typename T>
Point<T> variation39(const Point<T> &x) {
    auto t = theta(x);
    auto f = r(x);
    return Point(f * std::sin(t + f), f * std::cos(t - f));
}


#endif //FRACTALS_VARIATIONS_HPP
