#include <SDL.h>

#include "Variations.hpp"
#include "Flame.hpp"

const auto width = 800;
const auto height = 600;


int main(int argc, char *argv[]) {
    std::vector<unsigned char> nice_pixels;
    nice_pixels.resize(width * height * 3);
    bool quit = false;
    SDL_Event event;

    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window *window = SDL_CreateWindow("Flame",
                                          SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED,
                                          width,
                                          height,
                                          SDL_WINDOW_RESIZABLE);

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");
    SDL_Surface *surface = SDL_CreateRGBSurfaceFrom(
            nice_pixels.data(),
            width,
            height,
            24,
            3 * width,
            0xFF,
            0xFF << 8,
            0xFF << 16,
            0
    );

    class Val {
        std::mt19937 rng;
        std::uniform_real_distribution<float> dist1;
        std::uniform_real_distribution<float> dist2;
    public:
        Val() {
            std::random_device rd;
            rng.seed(rd());
            dist1 = std::uniform_real_distribution<float>(-1, 1);
            dist2 = std::uniform_real_distribution<float>(-0.2, 0.2);
        }

        float val = dist1(rng);
        float dir = dist2(rng);

        void step() {
            val += dir;
            if (val >= 1) {
                dir = dist2(rng);
                val = 1;
            } else if (val <= -1) {
                dir = dist2(rng);
                val = -1;
            }
        }
    };

    Val a, b, c, d, e;
    while (true) {
        FlameDescriptor descriptor(
                {FlameFunction(
                        {FlameVariation(
                                &variation2,
                                FlameCoeffs(
                                        -0.681206, a.val, 0.43268, -0.9542476, b.val, -0.995898
                                ),
                                1
                        )},
                        1,
                        0
                ), FlameFunction(
                        {FlameVariation(
                                &variation1,
                                FlameCoeffs(
                                        -0.681206, -0.0779465, 0.20769, c.val, -0.0416126, -0.262334
                                ),
                                1
                        )},
                        1,
                        0.66
                ), FlameFunction(
                        {FlameVariation(
                                &variation3,
                                FlameCoeffs(
                                        e.val, -0.0779465, 0.20769, 0.255065, -0.9542476, -0.262334
                                ),
                                1
                        )},
                        1,
                        0.33
                ), FlameFunction(
                        {FlameVariation(
                                &variation4,
                                FlameCoeffs(
                                        e.val, -0.0779465, 0.25769, 0.255065, -0.9542476, -0.262334
                                ),
                                0.4
                        ), FlameVariation(
                                &variation5,
                                FlameCoeffs(
                                        1, -0.0779465, 0.25769, 0.255065, -0.9542476, -0.362334
                                ),
                                0.6
                        )},
                        1,
                        1.0
                ), FlameFunction(
                        {FlameVariation(
                                &variation39,
                                FlameCoeffs(
                                        0.6, -0.4779465, 0.25769, 0.755065, -0.9542476, -0.062334
                                ),
                                0.4
                        )},
                        1,
                        1.0
                )},
                1000,
                5000,
                {-1.777, 1.777},
                {-1, 1},
                width,
                height,
                1,
                2.0,
                FlameColourRamp({
                                        {0.0,  FlameColour(255, 0, 33)},
                                        {0.5,  FlameColour(166, 30, 255)},
                                        {0.75, FlameColour(10, 165, 65)},
                                        {1.0,  FlameColour(0, 0, 255)},
                                })
        );

        a.step();
        b.step();
        c.step();
        e.step();

        Flame<float> flame(descriptor);

        auto image_rep = flame.flame();

        for (uint32_t y = 0; y < descriptor.height; y++) {
            for (uint32_t x = 0; x < descriptor.width; x++) {
                *(nice_pixels.data() + y * descriptor.width * 3 + x * 3 + 0) = image_rep[y][x].r;
                *(nice_pixels.data() + y * descriptor.width * 3 + x * 3 + 1) = image_rep[y][x].g;
                *(nice_pixels.data() + y * descriptor.width * 3 + x * 3 + 2) = image_rep[y][x].b;
            }
        }


        SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
        int w, h;
        SDL_GetWindowSize(window, &w, &h);
        SDL_Rect winsize;
        winsize.x = 0;
        winsize.y = 0;
        winsize.w = w;
        winsize.h = h;
        SDL_RenderCopy(renderer, texture, nullptr, &winsize);
        SDL_RenderPresent(renderer);
        SDL_DestroyTexture(texture);

        SDL_PollEvent(nullptr);
        if (SDL_HasEvent(SDL_QUIT)) {
            break;
        }
    }

    SDL_Quit();

    return 0;
}