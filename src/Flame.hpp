//
// Created by jonathan on 2019-06-08.
//

#ifndef FRACTALS_FLAME_HPP
#define FRACTALS_FLAME_HPP

#include "Point.hpp"

#include <cstdint>

#include <algorithm>
#include <iostream>
#include <random>
#include <thread>
#include <vector>

template<typename T>
struct FlameDescriptorGeneric {
    typedef Point<T> (*VariationFunction)(const Point<T> &);

    struct Coeffs {
        T a;
        T b;
        T c;
        T d;
        T e;
        T f;

        Coeffs(T a, T b, T c, T d, T e, T f) : a(a), b(b), c(c), d(d), e(e), f(f) {}
    };

    struct Variation {
        VariationFunction function;
        Coeffs coeffs;
        T weight;

        Variation(VariationFunction function, Coeffs coeffs, T weight) : function(function), coeffs(coeffs),
                                                                         weight(weight) {}
    };

    struct FlameFunction {
        std::vector<Variation> variation;
        uint32_t probability;
        T colour;

        FlameFunction(std::vector<Variation> variation, uint32_t probability, T colour) : variation(
                std::move(variation)), probability(probability), colour(colour) {}
    };

    std::vector<FlameFunction> functions;
    uint32_t n_iterations;
    uint32_t n_samples;

    std::pair<T, T> xrange;
    std::pair<T, T> yrange;

    uint32_t width;
    uint32_t height;

    uint32_t super_factor;

    T gamma;

    struct Colour {
        uint8_t r;
        uint8_t g;
        uint8_t b;

        Colour(uint8_t r, uint8_t g, uint8_t b) : r(r), g(g), b(b) {}

        template<typename A>
        Colour(A) : r(0), g(0), b(0) {}

        Colour() : r(0), g(0), b(0) {}
    };

    class ColourRamp {
        std::vector<std::pair<T, Colour>> m_colours;
    public:
        explicit ColourRamp(std::vector<std::pair<T, Colour>> colours) : m_colours(colours) {
            std::sort(colours.begin(), colours.end(), [](const std::pair<T, Colour> &a, const std::pair<T, Colour> &b) {
                return a.first - b.first;
            });

            if (m_colours.empty()) {
                m_colours.emplace_back(0.5, Colour(255, 255, 255));
            }
        }

        Colour get_colour(T index) {
            if (m_colours.size() < 2) {
                return m_colours[0].second;
            }

            auto it = m_colours.begin();
            while (it->first < index) {
                it++;
            }

            if (it == m_colours.begin()) {
                return it->second;
            }

            T val = index - (it - 1)->first;
            const auto &colour_a = (it - 1)->second;
            const auto &colour_b = it->second;

            return Colour(
                    colour_a.r + val * (colour_b.r - colour_a.r),
                    colour_a.g + val * (colour_b.g - colour_a.g),
                    colour_a.b + val * (colour_b.b - colour_a.b));
        }
    };

    ColourRamp colours;

    FlameDescriptorGeneric(
            std::vector<FlameFunction> functions,
            uint32_t n_iterations,
            uint32_t n_samples,
            std::pair<T, T> xrange,
            std::pair<T, T> yrange,
            uint32_t width,
            uint32_t height,
            uint32_t super_factor,
            T gamma,
            ColourRamp colours
    ) : functions(std::move(functions)), n_iterations(n_iterations), n_samples(n_samples), xrange(std::move(xrange)),
        yrange(std::move(yrange)), width(width), height(height), super_factor(super_factor), gamma(gamma),
        colours(std::move(colours)) {}
};


template<typename T>
class Flame {
private:
    using Colour = typename FlameDescriptorGeneric<T>::Colour;

public:
    explicit Flame(FlameDescriptorGeneric<T> descriptor) : m_descriptor(descriptor),
                                                           m_histogram(descriptor.width * descriptor.super_factor,
                                                                       descriptor.height * descriptor.super_factor),
                                                           m_colour(descriptor.width * descriptor.super_factor,
                                                                    descriptor.height * descriptor.super_factor) {
//        std::random_device rd;
        m_rng.seed(123456); // picks "noise" pattern
        make_prop_dist();
    }


private:

    Point<T>
    perform_iteration(const typename FlameDescriptorGeneric<T>::FlameFunction &function, const Point<T> &point) {
        Point<T> new_point((T) 0.0, (T) 0.0);
        for (const auto &variation : function.variation) {
            auto res = variation.function(
                    Point<T>(variation.coeffs.a * point.x() + variation.coeffs.b * point.y() + variation.coeffs.c,
                             variation.coeffs.d * point.x() + variation.coeffs.e * point.y() + variation.coeffs.e));

            new_point = Point<T>(variation.weight * res.x() + new_point.x(),
                                 variation.weight * res.y() + new_point.y());
        }
        return new_point;
    }

    std::pair<uint32_t, uint32_t>
    flame_space_to_histo_space(const Point<T> &point) {
        return std::make_pair(
                ((point.x() - m_descriptor.xrange.first) / (m_descriptor.xrange.second - m_descriptor.xrange.first) *
                 m_histogram.width()),
                ((point.y() - m_descriptor.yrange.first) / (m_descriptor.yrange.second - m_descriptor.yrange.first) *
                 m_histogram.height())
        );
    }

    void perform_sample_step() {
        Point<T> point(m_prob_startx(m_rng), m_prob_starty(m_rng));

        uint32_t iteration = 0;
        while (iteration++ < m_descriptor.n_iterations) {
            auto function_i = m_prob_dist(m_rng);
            const auto &function = m_descriptor.functions[function_i];
            point = perform_iteration(function, point);
            if (iteration > 20) {
                auto histo_space = flame_space_to_histo_space(point);

                if (histo_space.first >= m_histogram.width() || histo_space.second >= m_histogram.height()) {
                    continue;
                }

                m_histogram[histo_space.second][histo_space.first] += 1;

                auto old_colour = m_colour[histo_space.second][histo_space.first];
                m_colour[histo_space.second][histo_space.first] = (function.colour + old_colour) / 2;
            }
        }
    }

    void make_prop_dist() {
        std::vector<uint32_t> probs;
        probs.reserve(m_descriptor.functions.size());
        for (const auto &function : m_descriptor.functions) {
            probs.emplace_back(function.probability);
        }

        m_prob_dist = std::discrete_distribution<uint32_t>(probs.begin(), probs.end());
        m_prob_startx = std::uniform_real_distribution<T>(m_descriptor.xrange.first, m_descriptor.xrange.second);
        m_prob_starty = std::uniform_real_distribution<T>(m_descriptor.yrange.first, m_descriptor.yrange.second);
    }

    template<typename E>
    class Histogram {
    public:
        Histogram(uint32_t width, uint32_t height) : m_width(width), m_height(height) {
            m_storage.reserve(width * height);
            for (auto i = 0; i < width * height; i++) {
                m_storage.emplace_back(0);
            }
        }

        E *operator[](uint32_t row) {
            return m_storage.data() + row * m_width;
        }

        const E *operator[](uint32_t row) const {
            return m_storage.data() + row * m_width;
        }

        uint32_t width() { return m_width; }

        uint32_t height() { return m_height; }

    private:
        std::vector<E> m_storage;
        uint32_t m_width;
        uint32_t m_height;
    };

    std::uniform_real_distribution<T> m_prob_startx;
    std::uniform_real_distribution<T> m_prob_starty;
    std::discrete_distribution<uint32_t> m_prob_dist;
    std::mt19937 m_rng;
    FlameDescriptorGeneric<T> m_descriptor;

    Histogram<uint32_t> m_histogram;
    Histogram<T> m_colour;

    template<typename E>
    Histogram<T> reduce_histogram(const Histogram<E> &histogram) {
        Histogram<T> new_hist(m_descriptor.width, m_descriptor.height);
        auto den = m_descriptor.super_factor * m_descriptor.super_factor;

        for (uint32_t x = 0; x < m_descriptor.width; x++) {
            for (uint32_t y = 0; y < m_descriptor.height; y++) {
                T ha = 0.0;
                for (uint32_t j = 0; j < m_descriptor.super_factor; j++) {
                    for (uint32_t k = 0; k < m_descriptor.super_factor; k++) {
                        ha += histogram[y * m_descriptor.super_factor + k][x * m_descriptor.super_factor + j];
                    }
                }
                new_hist[y][x] = ha / den;

            }
        }
        return new_hist;
    }

    uint32_t histogram_largest() {
        uint32_t largest = 0;
        for (uint32_t x = 0; x < m_descriptor.width; x++) {
            for (uint32_t y = 0; y < m_descriptor.height; y++) {
                auto v = m_histogram[y][x];
                if (v > largest) {
                    largest = v;
                }
            }
        }
        return largest;
    }

public:
    Histogram<Colour> flame(size_t n_threads = std::thread::hardware_concurrency() - 1) {
        std::vector<std::thread> threads;

        for (auto i = 0; i < n_threads; i++) {
            threads.push_back(std::thread([&]() {
                for (auto sample = 0; sample < m_descriptor.n_samples / n_threads; sample++) {
                    perform_sample_step();
                }
            }));
        }

        for (auto &thread : threads) {
            if (thread.joinable()) {
                thread.join();
            };
        }

        auto reduced_histogram = reduce_histogram(m_histogram);
        auto reduced_colour = reduce_histogram(m_colour);
        Histogram<Colour> out_colour(m_descriptor.width, m_descriptor.height);
        auto log_largest = std::log2(histogram_largest());

        auto gamma_factor = (T) 1.0 / m_descriptor.gamma;

        for (uint32_t x = 0; x < m_descriptor.width; x++) {
            for (uint32_t y = 0; y < m_descriptor.height; y++) {
                auto bucket_value = reduced_histogram[y][x];
                auto alpha = std::log2(bucket_value) / log_largest;
                auto gca = std::pow(alpha, gamma_factor);

                auto colour_index = reduced_colour[y][x];
                auto mapped_colour = m_descriptor.colours.get_colour(colour_index);
                out_colour[y][x] = Colour(
                        (uint8_t) (((T) mapped_colour.r) * gca),
                        (uint8_t) (((T) mapped_colour.g) * gca),
                        (uint8_t) (((T) mapped_colour.b) * gca));

            }
        }
        return out_colour;
    }
};

using FlameDescriptor = FlameDescriptorGeneric<float>;
using FlameFunction = FlameDescriptor::FlameFunction;
using FlameVariation = FlameDescriptor::Variation;
using FlameCoeffs = FlameDescriptor::Coeffs;
using FlameColour = FlameDescriptor::Colour;
using FlameColourRamp = FlameDescriptor::ColourRamp;

#endif //FRACTALS_FLAME_HPP

